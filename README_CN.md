<h1 align="center"><a href="https://github.com/shalousun/smart-doc" target="_blank">Smart-Doc Project</a></h1>

## Introduce
smart-doc是一个java restful api文档生成工具，smart-doc颠覆了传统类似swagger这种大量采用注解侵入来生成文档的实现方法。
smart-doc完全基于接口源码分析来生成接口文档，完全做到零注解侵入，你只需要按照java标准注释的写，smart-doc就能帮你生成一个简易明了的markdown
或是一个像GitBook样式的静态html文档。如果你已经厌倦了swagger等文档工具的无数注解和强侵入污染，那请拥抱smart-doc吧！
## Features
- 零注解、零学习成本、只需要写标准java注释。
- 基于源代码接口定义自动推导，强大的返回结构推导。
- 支持Spring MVC,Spring Boot,Spring Boot Web Flux(controller书写方式)。
- 支持Callable,Future,CompletableFuture等异步接口返回的推导。
- 支持JavaBean上的JSR303参数校验规范。
- 对json请求参数的接口能够自动生成模拟json参数。
- 对一些常用字段定义能够生成有效的模拟值。
- 支持生成json返回值示例。
- 支持从项目外部加载源代码来生成字段注释(包括标准规范发布的jar包)。
- 支持生成多种格式文档：Markdown、HTML5、Asciidoctor。
- 轻易实现在Spring Boot服务上在线查看静态HTML5 api文档。
- 开放文档数据，可自由实现接入文档管理系统。
- 支持导出错误码和定义在代码中的各种字典码到接口文档。
## Getting started
smart-doc使用和测试可参考[smart-doc demo](https://gitee.com/sunyurepository/api-doc-test.git)。
```
# git clone https://gitee.com/sunyurepository/api-doc-test.git
```
你可以启动这个Spring Boot的项目，然后访问`http://localhost:8080/doc/api.html`来浏览smart-doc生成的接口文档。
### Dependency
#### maven
```
<dependency>
    <groupId>com.github.shalousun</groupId>
    <artifactId>smart-doc</artifactId>
    <version>1.7.7</version>
    <scope>test</scope>
</dependency>
```
#### gradle
```
testCompile 'com.github.shalousun:smart-doc:1.7.7'
```
### Create a unit test
通过运行一个单元测试来让Smart-doc为你生成一个简洁明了的api文档
```
public class SmartDocTest {
    private DocTemplate docTemplate;

    @Before
    public void before() {
        ApiConfig config = new ApiConfig();
        config.setServerUrl("http://localhost:8080"); // 项目地址
        config.setProjectName("test");// 设置项目名(非必须)，如果不设置会导致在使用一些自动添加标题序号的工具显示的序号不正常
        config.setStrict(true);// true会严格要求注释，推荐设置true
        config.setMd5EncryptedHtmlName(true);// 生成html时加密文档名不暴露controller的名称
        config.setAllInOne(true);// 指定文档输出路径
        config.setPackageFilters("com.xxx.controller"); // 指定包路径
        config.setCoverOld(true);// 如果该选项的值为false,则smart-doc生成allInOne.md文件的名称会自动添加版本号
        config.setOutPath("target/doc");
        BuilderConfig builderConfig = new BuilderConfig();
        RespListResolver respListResolver = new RespListResolver();
        builderConfig.getResponseParamHandler().addTypeResolver(respListResolver); // 将Resp<List<T>>转成array类型

        // 设置请求头，如果没有请求头，可以不用设置
        config.setRequestHeaders(
                ApiReqHeader.header().setName("userToken").setType("string").setDesc("用户标识token").setRequired(true),
                ApiReqHeader.header().setName("nonceStr").setType("string").setDesc("随机字符串").setRequired(true),
                ApiReqHeader.header().setName("sign").setType("string").setDesc("签名").setRequired(true),
                ApiReqHeader.header().setName("timeUnit").setType("string").setDesc("校验时间戳，时效性：5分钟").setRequired(true));

        // 对于外部jar的类，编译后注释会被擦除，无法获取注释，但是如果量比较多请使用setSourcePaths来加载外部代码
        // 如果有这种场景，则自己添加字段和注释，api-doc后期遇到同名字段则直接给相应字段加注释
        config.setCustomResponseFields(CustomRespField.field().setName("code").setDesc("成功返回：0，失败返回：1或其他数字"),
                CustomRespField.field().setName("msg").setDesc("错误信息"),
                CustomRespField.field().setName("data").setDesc("接口响应数据"),
                CustomRespField.field().setName("id").setDesc("自增id"),
                CustomRespField.field().setName("createTime").setDesc("创建时间"),
                CustomRespField.field().setName("updateTime").setDesc("更新时间"));
        docTemplate = new DocTemplate(config, builderConfig);
    }

    @Test
    public void testHtmlApi() {
        long start = System.currentTimeMillis();
        docTemplate.create(new HtmlDocCreator());
        long end = System.currentTimeMillis();
        DateTimeUtil.printRunTime(end, start);
    }

    @Test
    public void testControllerADocApi() {
        long start = System.currentTimeMillis();
        docTemplate.createControllerApi(new AdocDocCreator(), "FormServerController");
        long end = System.currentTimeMillis();
        DateTimeUtil.printRunTime(end, start);
    }

    @Test
    public void testADocApi() {
        long start = System.currentTimeMillis();
        docTemplate.create(new AdocDocCreator());
        long end = System.currentTimeMillis();
        DateTimeUtil.printRunTime(end, start);
    }

    @Test
    public void testControllerMDDocApi() {
        long start = System.currentTimeMillis();
        docTemplate.createControllerApi(new MarkDownDocCreator(), "FormDataServerController");
        long end = System.currentTimeMillis();
        DateTimeUtil.printRunTime(end, start);
    }

    @Test
    public void testMDDocApi() {
        long start = System.currentTimeMillis();
        docTemplate.create(new MarkDownDocCreator());
        long end = System.currentTimeMillis();
        DateTimeUtil.printRunTime(end, start);
    }
}
```
### Generated document example
#### 接口头部效果图
![输入图片说明](https://images.gitee.com/uploads/images/2018/0905/173104_abcf4345_144669.png "1.png")
#### 请求参数示例效果图
![请求参数示例](https://images.gitee.com/uploads/images/2018/0905/172510_853735b9_144669.png "2.png")
#### 响应参数示例效果图
![响应参数示例](https://images.gitee.com/uploads/images/2018/0905/172538_1918820c_144669.png "3.png")
## Building
如果你需要自己构建，那可以使用下面命令，构建需要依赖Java 1.8。
```
mvn clean install -Dmaven.test.skip=true
```
## Releases
[发布记录](https://gitee.com/sunyurepository/smart-doc/blob/master/RELEASE.md)
## Other reference
- [smart-doc功能使用介绍](https://my.oschina.net/u/1760791/blog/2250962)
- [smart-doc wiki](https://gitee.com/sunyurepository/smart-doc/wikis/Home?sort_id=1652800)
## License
Smart-doc is under the Apache 2.0 license.  See the [LICENSE](https://gitee.com/sunyurepository/smart-doc/blob/master/license.txt) file for details.
## Contact
愿意参与构建smart-doc或者是需要交流问题可以加入qq群：

QQ群： 170651381
