package com.power.doc.util;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

@Data
public class RespList<T> implements Serializable {
    /**
     * 错误码
     */
    private int code;
    /**
     * 错误信息
     */
    private String msg;
    /**
     * 成功响应数据，失败为null
     */
    private List<T> data;
    /**
     * 测试
     */
    private List<Integer> testData;
}
