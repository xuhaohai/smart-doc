package com.power.doc.util;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 统一响应结果
 */
@Data
public class Resp<T> implements Serializable {
    /**
     * 错误码
     */
    private int code;
    /**
     * 错误信息
     */
    private String msg;
    /**
     * 成功响应数据，失败为null
     */
    private T data;
}
