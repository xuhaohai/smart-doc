package com.power.doc.util;

import com.bc.plugin.doc.resolver.IResponseTypeResolver;
import com.power.doc.constants.DocGlobalConstants;
import com.power.doc.model.ApiReturn;

public class RespListResolver implements IResponseTypeResolver {
    private static final String RESP_LIST_PRE_NAME = "com.xxx.xxx.xxx.Resp<" + DocGlobalConstants.JAVA_LIST_FULLY;
    private static final String RESP_LIST_NAME = "com.power.doc.util.RespList";

    @Override
    public ApiReturn processReturnType(String fullyName) {
        ApiReturn apiReturn = new ApiReturn();
        fullyName = fullyName.replaceFirst(RESP_LIST_PRE_NAME, RESP_LIST_NAME);
        fullyName = fullyName.substring(0, fullyName.lastIndexOf(">"));
        apiReturn.setGenericCanonicalName(fullyName);
        apiReturn.setSimpleName(fullyName.substring(0, fullyName.indexOf("<")));
        return apiReturn;
    }

    @Override
    public boolean support(String fullyName) {
        return fullyName.startsWith(RESP_LIST_PRE_NAME);
    }
}
