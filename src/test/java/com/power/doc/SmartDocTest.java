package com.power.doc;

import com.bc.plugin.doc.builder.BuilderConfig;
import com.bc.plugin.doc.template.DocTemplate;
import com.bc.plugin.doc.template.impl.AdocDocCreator;
import com.bc.plugin.doc.template.impl.HtmlDocCreator;
import com.bc.plugin.doc.template.impl.MarkDownDocCreator;
import com.power.common.util.DateTimeUtil;
import com.power.doc.model.ApiConfig;
import com.power.doc.model.ApiReqHeader;
import com.power.doc.model.CustomRespField;
import com.power.doc.util.RespListResolver;
import org.junit.Before;
import org.junit.Test;

/**
 * @author xhh
 */
public class SmartDocTest {
    private DocTemplate docTemplate;

    @Before
    public void before() {
        ApiConfig config = new ApiConfig();
        config.setServerUrl("http://localhost:8080"); // 项目地址
        config.setProjectName("test");// 设置项目名(非必须)，如果不设置会导致在使用一些自动添加标题序号的工具显示的序号不正常
        config.setStrict(true);// true会严格要求注释，推荐设置true
        config.setMd5EncryptedHtmlName(true);// 生成html时加密文档名不暴露controller的名称
        config.setAllInOne(true);// 指定文档输出路径
        config.setPackageFilters("com.xxx.controller"); // 指定包路径
        config.setCoverOld(true);// 如果该选项的值为false,则smart-doc生成allInOne.md文件的名称会自动添加版本号
        config.setOutPath("target/doc");
        BuilderConfig builderConfig = new BuilderConfig();
        RespListResolver respListResolver = new RespListResolver();
        builderConfig.getResponseParamHandler().addTypeResolver(respListResolver); // 将Resp<List<T>>转成array类型

        // 设置请求头，如果没有请求头，可以不用设置
        config.setRequestHeaders(
                ApiReqHeader.header().setName("userToken").setType("string").setDesc("用户标识token").setRequired(true),
                ApiReqHeader.header().setName("nonceStr").setType("string").setDesc("随机字符串").setRequired(true),
                ApiReqHeader.header().setName("sign").setType("string").setDesc("签名").setRequired(true),
                ApiReqHeader.header().setName("timeUnit").setType("string").setDesc("校验时间戳，时效性：5分钟").setRequired(true));

        // 对于外部jar的类，编译后注释会被擦除，无法获取注释，但是如果量比较多请使用setSourcePaths来加载外部代码
        // 如果有这种场景，则自己添加字段和注释，api-doc后期遇到同名字段则直接给相应字段加注释
        config.setCustomResponseFields(CustomRespField.field().setName("code").setDesc("成功返回：0，失败返回：1或其他数字"),
                CustomRespField.field().setName("msg").setDesc("错误信息"),
                CustomRespField.field().setName("data").setDesc("接口响应数据"),
                CustomRespField.field().setName("id").setDesc("自增id"),
                CustomRespField.field().setName("createTime").setDesc("创建时间"),
                CustomRespField.field().setName("updateTime").setDesc("更新时间"));
        docTemplate = new DocTemplate(config, builderConfig);
    }

    @Test
    public void testHtmlApi() {
        long start = System.currentTimeMillis();
        docTemplate.create(new HtmlDocCreator());
        long end = System.currentTimeMillis();
        DateTimeUtil.printRunTime(end, start);
    }


    @Test
    public void testADocApi() {
        long start = System.currentTimeMillis();
        docTemplate.create(new AdocDocCreator());
        long end = System.currentTimeMillis();
        DateTimeUtil.printRunTime(end, start);
    }


    @Test
    public void testMDDocApi() {
        long start = System.currentTimeMillis();
        docTemplate.create(new MarkDownDocCreator());
        long end = System.currentTimeMillis();
        DateTimeUtil.printRunTime(end, start);
    }
}
