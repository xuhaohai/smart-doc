package com.power.doc.model;

/**
 * @author yu 2019/10/31.
 */
public class ApiDataDictionary {

    /**
     * Dictionary
     */
    private String title;

    /**
     * enumClass
     */
    private Class<? extends Enum> enumClass;


    /**
     * code field
     */
    private String codeField;

    /**
     * description field
     */
    private String descField;


    public static ApiDataDictionary dict() {
        return new ApiDataDictionary();
    }

    public String getTitle() {
        return title;
    }

    public ApiDataDictionary setTitle(String title) {
        this.title = title;
        return this;
    }

    public Class getEnumClass() {
        return enumClass;
    }

    public ApiDataDictionary setEnumClass(Class<? extends Enum> enumClass) {
        this.enumClass = enumClass;
        return this;
    }

    public String getCodeField() {
        return codeField;
    }

    public ApiDataDictionary setCodeField(String codeField) {
        this.codeField = codeField;
        return this;
    }

    public String getDescField() {
        return descField;
    }

    public ApiDataDictionary setDescField(String descField) {
        this.descField = descField;
        return this;
    }

}
