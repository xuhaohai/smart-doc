package com.power.doc.constants;

/**
 * java annotations
 *
 * @author yu 2019/9/19.
 */
public class DocAnnotationConstants {

    public static final String SHORT_CONTROLLER = "Controller";

    public static final String SHORT_REST_CONTROLLER = "RestController";

    public static final String SHORT_PATH_VARIABLE = "PathVariable";

    public static final String SHORT_REQ_PARAM = "RequestParam";

    public static final String SHORT_JSON_IGNORE = "JsonIgnore";

    public static final String SHORT_JSON_PROPERTY = "JsonProperty";

    public static final String SHORT_JSON_FIELD = "JSONField";

    public static final String REQUIRED_PROP = "required";

    public static final String SERIALIZE_PROP = "serialize";

    public static final String NAME_PROP = "name";

    public static final String VALUE_PROP = "value";

    public static final String DEFAULT_VALUE_PROP = "defaultValue";

    public static final String GET_MAPPING = "GetMapping";

    public static final String POST_MAPPING = "PostMapping";

    public static final String PUT_MAPPING = "PutMapping";

    public static final String DELETE_MAPPING = "DeleteMapping";

    public static final String REQUEST_MAPPING = "RequestMapping";

    public static final String REQUEST_BODY = "RequestBody";

    public static final String JSON_PARAM = "JsonParam";

    public static final String REQUEST_HERDER = "RequestHeader";

    public static final String REQUEST_PARAM = "RequestParam";


}
