package com.bc.plugin.doc.template.base;

import com.bc.plugin.doc.builder.BuilderConfig;
import com.bc.plugin.doc.template.impl.hanlde.HtmlApiParamHandle;
import com.power.doc.model.ApiConfig;

public abstract class BaseHtmlCreator implements IDocCreator {

    @Override
    public void beforeBuild(ApiConfig apiConfig, BuilderConfig builderConfig) {
        builderConfig.getApiParamHandles().add(new HtmlApiParamHandle());
    }
}
