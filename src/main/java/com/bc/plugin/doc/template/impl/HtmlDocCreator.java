package com.bc.plugin.doc.template.impl;

import com.bc.plugin.doc.dto.TemplateDataDTO;
import com.bc.plugin.doc.template.base.BaseHtmlCreator;
import com.power.common.util.CollectionUtil;
import com.power.common.util.DateTimeUtil;
import com.power.common.util.FileUtil;
import com.power.doc.constants.TemplateVariable;
import com.power.doc.model.ApiDoc;
import com.power.doc.model.ApiDocDict;
import com.power.doc.model.ApiErrorCode;
import com.power.doc.utils.BeetlTemplateUtil;
import com.power.doc.utils.MarkDownUtil;
import org.beetl.core.Template;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static com.power.doc.constants.DocGlobalConstants.*;

public class HtmlDocCreator extends BaseHtmlCreator {
    private static long now = System.currentTimeMillis();

    @Override
    public void create(TemplateDataDTO templateDataDTO, String outFilePath) {
        if (templateDataDTO.isAllInOne()) {
            Template indexCssTemplate = BeetlTemplateUtil.getByName(ALL_IN_ONE_CSS);
            FileUtil.nioWriteFile(indexCssTemplate.render(), templateDataDTO.getOutPath() + FILE_SEPARATOR + ALL_IN_ONE_CSS);
            Template tpl = BeetlTemplateUtil.getByName(ALL_IN_ONE_HTML_TPL);
            tpl.binding(templateDataDTO.toMap());
            FileUtil.nioWriteFile(tpl.render(), outFilePath);
        } else {
            if (CollectionUtil.isNotEmpty(templateDataDTO.getApiDocList())) {
                Template indexTemplate = BeetlTemplateUtil.getByName(INDEX_TPL);
                indexTemplate.binding(templateDataDTO.toMap());
                ApiDoc doc = templateDataDTO.getApiDocList().get(0);
                String homePage = doc.getAlias();
                indexTemplate.binding(TemplateVariable.HOME_PAGE.getVariable(), homePage);
                FileUtil.nioWriteFile(indexTemplate.render(), outFilePath);
            }
            copyCss(templateDataDTO.getOutPath());
            buildApiDoc(templateDataDTO.getApiDocList(), templateDataDTO.getOutPath());
            buildErrorCodeDoc(templateDataDTO.getErrorCodes(), templateDataDTO.getOutPath());
            buildDictionary(templateDataDTO.getApiDocDictList(), templateDataDTO.getOutPath());
        }
        try {
            System.out.println(new File(outFilePath).getCanonicalPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String fileExtension() {
        return ".html";
    }

    private static void copyCss(String outPath) {
        Template indexCssTemplate = BeetlTemplateUtil.getByName(INDEX_CSS_TPL);
        Template mdCssTemplate = BeetlTemplateUtil.getByName(MARKDOWN_CSS_TPL);
        FileUtil.nioWriteFile(indexCssTemplate.render(), outPath + FILE_SEPARATOR + INDEX_CSS_TPL);
        FileUtil.nioWriteFile(mdCssTemplate.render(), outPath + FILE_SEPARATOR + MARKDOWN_CSS_TPL);
    }

    /**
     * build ever controller api
     *
     * @param apiDocList list of api doc
     * @param outPath    output path
     */
    private static void buildApiDoc(List<ApiDoc> apiDocList, String outPath) {
        FileUtil.mkdirs(outPath);
        Template htmlApiDoc;
        String strTime = DateTimeUtil.long2Str(now, DateTimeUtil.DATE_FORMAT_SECOND);
        for (ApiDoc doc : apiDocList) {
            Template apiTemplate = BeetlTemplateUtil.getByName(API_DOC_MD_TPL);
            apiTemplate.binding(TemplateVariable.ORDER.getVariable(), doc.getOrder());
            apiTemplate.binding(TemplateVariable.DESC.getVariable(), doc.getDesc());
            apiTemplate.binding(TemplateVariable.NAME.getVariable(), doc.getName());
            apiTemplate.binding(TemplateVariable.LIST.getVariable(), doc.getList());// 类名

            String html = MarkDownUtil.toHtml(apiTemplate.render());
            htmlApiDoc = BeetlTemplateUtil.getByName(HTML_API_DOC_TPL);
            htmlApiDoc.binding(TemplateVariable.HTML.getVariable(), html);
            htmlApiDoc.binding(TemplateVariable.TITLE.getVariable(), doc.getDesc());
            htmlApiDoc.binding(TemplateVariable.CREATE_TIME.getVariable(), strTime);
            htmlApiDoc.binding(TemplateVariable.VERSION.getVariable(), now);
            FileUtil.nioWriteFile(htmlApiDoc.render(), outPath + FILE_SEPARATOR + doc.getAlias() + ".html");
        }
    }

    /**
     * build error_code html
     *
     * @param errorCodeList list of error code
     * @param outPath
     */
    private static void buildErrorCodeDoc(List<ApiErrorCode> errorCodeList, String outPath) {
        if (CollectionUtil.isNotEmpty(errorCodeList)) {
            Template error = BeetlTemplateUtil.getByName(ERROR_CODE_LIST_MD_TPL);
            error.binding(TemplateVariable.LIST.getVariable(), errorCodeList);
            Template template = binding(error, ERROR_CODE_LIST_EN_TITLE);
            FileUtil.nioWriteFile(template.render(), outPath + FILE_SEPARATOR + "error_code.html");
        }
    }

    /**
     * build dictionary
     *
     * @param apiDocDictList dictionary list
     * @param outPath
     */
    private static void buildDictionary(List<ApiDocDict> apiDocDictList, String outPath) {
        if (CollectionUtil.isNotEmpty(apiDocDictList)) {
            Template template = BeetlTemplateUtil.getByName(DICT_LIST_MD_TPL);
            template.binding(TemplateVariable.DICT_LIST.getVariable(), apiDocDictList);
            template = binding(template, DICT_EN_TITLE);
            FileUtil.nioWriteFile(template.render(), outPath + FILE_SEPARATOR + "dict.html");
        }
    }

    private static Template binding(Template htmlTemplate, String title) {
        String templateHtml = MarkDownUtil.toHtml(htmlTemplate.render());
        Template template = BeetlTemplateUtil.getByName(HTML_API_DOC_TPL);
        template.binding(TemplateVariable.VERSION.getVariable(), now);
        template.binding(TemplateVariable.TITLE.getVariable(), title);
        template.binding(TemplateVariable.HTML.getVariable(), templateHtml);
        template.binding(TemplateVariable.CREATE_TIME.getVariable(),
                DateTimeUtil.long2Str(now, DateTimeUtil.DATE_FORMAT_SECOND));
        return template;
    }

}
