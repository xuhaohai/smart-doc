package com.bc.plugin.doc.template.impl;

import com.bc.plugin.doc.dto.TemplateDataDTO;
import com.bc.plugin.doc.template.base.BaseHtmlCreator;
import com.power.common.util.CollectionUtil;
import com.power.common.util.FileUtil;
import com.power.doc.constants.TemplateVariable;
import com.power.doc.model.ApiDoc;
import com.power.doc.utils.BeetlTemplateUtil;
import org.beetl.core.Template;

import java.io.File;
import java.io.IOException;

import static com.power.doc.constants.DocGlobalConstants.*;

public class MarkDownDocCreator extends BaseHtmlCreator {

    @Override
    public void create(TemplateDataDTO templateDataDTO, String outFilePath) {
        if (templateDataDTO.isAllInOne()) {
            Template tpl = BeetlTemplateUtil.getByName(getAllInOneTpl());
            tpl.binding(templateDataDTO.toMap());
            FileUtil.nioWriteFile(tpl.render(), outFilePath);
        } else {
            for (ApiDoc doc : templateDataDTO.getApiDocList()) {
                Template mapper = BeetlTemplateUtil.getByName(getApiDocTpl());
                mapper.binding(TemplateVariable.DESC.getVariable(), doc.getDesc());
                mapper.binding(TemplateVariable.NAME.getVariable(), doc.getName());
                mapper.binding(TemplateVariable.LIST.getVariable(), doc.getList());
                FileUtil.nioWriteFile(mapper.render(), templateDataDTO.getOutPath() + FILE_SEPARATOR + doc.getName() + fileExtension());
            }
            if (CollectionUtil.isNotEmpty(templateDataDTO.getErrorCodes())) {
                Template mapper = BeetlTemplateUtil.getByName(getErrorCodeTpl());
                mapper.binding(TemplateVariable.LIST.getVariable(), templateDataDTO.getErrorCodes());
                FileUtil.nioWriteFile(mapper.render(), templateDataDTO.getOutPath() + FILE_SEPARATOR + ERROR_CODE_LIST_MD);
            }
        }
        try {
            System.out.println(new File(outFilePath).getCanonicalPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String fileExtension() {
        return ".md";
    }

    public String getAllInOneTpl() {
        return ALL_IN_ONE_MD_TPL;
    }

    public String getApiDocTpl() {
        return API_DOC_MD_TPL;
    }

    public String getErrorCodeTpl() {
        return ERROR_CODE_LIST_MD_TPL;
    }
}
