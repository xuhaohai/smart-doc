package com.bc.plugin.doc.template.impl.hanlde;

import com.bc.plugin.doc.dto.FieldClassDTO;
import com.bc.plugin.doc.resolver.IApiParamHandle;
import com.power.common.util.StringUtil;
import com.power.doc.constants.DocGlobalConstants;
import com.power.doc.model.ApiParam;

/**
 * @author xhh
 */
public class HtmlApiParamHandle implements IApiParamHandle {

    @Override
    public ApiParam handle(ApiParam apiParam, FieldClassDTO fieldClassDTO, boolean isResponse) {
        String comment = apiParam.getDesc();
        if (StringUtil.isNotEmpty(comment)) {
            comment = comment.replaceAll("\r\n", "<br>");
            comment = comment.replaceAll("\n", "<br>");
            apiParam.setDesc(comment);
        }
        return apiParam;
    }

    @Override
    public void handleApiParam(ApiParam apiParam) {
        if (apiParam.getLevel() > 0) {
            StringBuilder preBuilder = new StringBuilder();
            for (int j = 1; j < apiParam.getLevel(); j++) {
                preBuilder.append(DocGlobalConstants.FIELD_SPACE);
            }
            preBuilder.append("└─");
            apiParam.setField(preBuilder.toString() + apiParam.getField());
        }
        if (StringUtil.isEmpty(apiParam.getDesc())) {
            apiParam.setDesc(DocGlobalConstants.NO_COMMENTS_FOUND);
        }
    }
}
