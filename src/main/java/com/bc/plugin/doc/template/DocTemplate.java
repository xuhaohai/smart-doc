package com.bc.plugin.doc.template;

import com.bc.plugin.doc.builder.ApiDocDataBuilder;
import com.bc.plugin.doc.builder.BuilderConfig;
import com.bc.plugin.doc.dto.TemplateDataDTO;
import com.bc.plugin.doc.resolver.DictionaryResolver;
import com.bc.plugin.doc.template.base.IDocCreator;
import com.power.common.util.FileUtil;
import com.power.doc.model.ApiConfig;
import com.power.doc.model.ApiDoc;
import com.power.doc.model.ApiDocDict;
import lombok.AllArgsConstructor;

import java.util.Arrays;
import java.util.List;

/**
 * @author xhh
 */
@AllArgsConstructor
public class DocTemplate {
    private ApiConfig apiConfig;
    private BuilderConfig builderConfig;

    public DocTemplate(ApiConfig apiConfig) {
        this.apiConfig = apiConfig;
        this.builderConfig = new BuilderConfig();
    }


    /**
     * 生成文档，默认文件名：api
     */
    public void create(IDocCreator docCreator) {
        create(docCreator, "api");
    }

    /**
     * 生成指定controller的文档，保存在一个文件中
     *
     * @param fileName 文档文档名
     */
    public void create(IDocCreator docCreator, String fileName) {
        create(docCreator, fileName, null);
    }

    /**
     * 生成指定controller的文档，保存在一个文件中
     *
     * @param controllerName 类路径，fullName，不为NULL时，生成指定controller的文档
     */
    public void create(IDocCreator docCreator, String fileName, String controllerName) {
        builderConfig.initData(apiConfig);
        docCreator.beforeBuild(apiConfig, builderConfig);
        ApiDocDataBuilder apiDocDataBuilder = new ApiDocDataBuilder(apiConfig, builderConfig);
        List<ApiDoc> apiDocList;
        if (controllerName != null) {
            if (apiConfig.getPackageFilters() != null && !controllerName.contains(".")) {
                controllerName = apiConfig.getPackageFilters() + "." + controllerName;
            }
            apiDocList = Arrays.asList(apiDocDataBuilder.getSingleControllerApiData(controllerName));
        } else {
            apiDocList = apiDocDataBuilder.getControllerApiData();
        }
        List<ApiDocDict> apiDocDictList = DictionaryResolver.buildDictionary(apiConfig.getDataDictionaries(), builderConfig.getJavaClassFileBuilder().getJavaProjectBuilder());
        TemplateDataDTO templateDataDTO = TemplateDataDTO.builder()
                .allInOne(apiConfig.isAllInOne())
                .apiDocDictList(apiDocDictList)
                .apiDocList(apiDocList)
                .docLanguage(apiConfig.getLanguage())
                .errorCodes(apiConfig.getErrorCodes())
                .fileName(fileName)
                .outPath(apiConfig.getOutPath())
                .projectName(apiConfig.getProjectName())
                .revisionLogs(apiConfig.getRevisionLogs())
                .build();
        FileUtil.mkdirs(templateDataDTO.getOutPath());
        docCreator.create(templateDataDTO);

    }

}
