package com.bc.plugin.doc.template.impl;

import com.bc.plugin.doc.dto.TemplateDataDTO;

import static com.power.doc.constants.DocGlobalConstants.*;

public class AdocDocCreator extends MarkDownDocCreator {

    @Override
    public void create(TemplateDataDTO templateDataDTO, String outFilePath) {
        templateDataDTO.getApiDocList().forEach(apiDoc ->
                apiDoc.getList().forEach(apiMethodDoc -> apiMethodDoc.setHeaders("|" + apiMethodDoc.getHeaders()))
        );
        super.create(templateDataDTO);
    }

    @Override
    public String fileExtension() {
        return ".adoc";
    }

    @Override
    public String getAllInOneTpl() {
        return ALL_IN_ONE_ADOC_TPL;
    }

    @Override
    public String getApiDocTpl() {
        return API_DOC_ADOC_TPL;
    }

    @Override
    public String getErrorCodeTpl() {
        return ERROR_CODE_LIST_ADOC_TPL;
    }
}
