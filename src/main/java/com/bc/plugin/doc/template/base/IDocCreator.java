package com.bc.plugin.doc.template.base;

import com.bc.plugin.doc.builder.BuilderConfig;
import com.bc.plugin.doc.dto.TemplateDataDTO;
import com.power.doc.model.ApiConfig;

import static com.power.doc.constants.DocGlobalConstants.FILE_SEPARATOR;

/**
 * @author xhh
 */
public interface IDocCreator {

    /**
     * 生成实现，保存到一个文件中
     */
    void create(TemplateDataDTO templateDataDTO, String outFilePath);

    /**
     * 文件后缀
     */
    String fileExtension();

    default void create(TemplateDataDTO templateDataDTO) {
        String outFilePath = getFilePath(templateDataDTO);
        create(templateDataDTO, outFilePath);
    }

    default void beforeBuild(ApiConfig apiConfig, BuilderConfig builderConfig) {
    }

    default String getFilePath(TemplateDataDTO templateDataDTO) {
        String fileName = templateDataDTO.getFileName();
        if (!fileName.contains(".")) {
            fileName = templateDataDTO.getFileName() + fileExtension();
        }
        String outFilePath = templateDataDTO.getOutPath() + FILE_SEPARATOR + fileName;
        return outFilePath;
    }
}
