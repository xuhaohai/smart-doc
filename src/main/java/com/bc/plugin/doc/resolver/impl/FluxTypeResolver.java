package com.bc.plugin.doc.resolver.impl;

import com.bc.plugin.doc.resolver.IResponseTypeResolver;
import com.power.doc.constants.DocGlobalConstants;
import com.power.doc.model.ApiReturn;

/**
 * @author xhh 
 */
public class FluxTypeResolver implements IResponseTypeResolver {

	@Override
	public ApiReturn processReturnType(String fullyName) {
		fullyName = fullyName.replace("reactor.core.publisher.Flux", DocGlobalConstants.JAVA_LIST_FULLY);
		ApiReturn apiReturn = new ApiReturn();
		apiReturn.setGenericCanonicalName(fullyName);
		apiReturn.setSimpleName(DocGlobalConstants.JAVA_LIST_FULLY);
		return apiReturn;
	}

	@Override
	public boolean support(String fullyName) {
		return fullyName.startsWith("reactor.core.publisher.Flux");
	}

}
