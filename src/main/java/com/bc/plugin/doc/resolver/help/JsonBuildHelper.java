package com.bc.plugin.doc.resolver.help;

import com.bc.plugin.doc.builder.JavaClassFileBuilder;
import com.power.common.util.JsonFormatUtil;
import com.power.common.util.StringUtil;
import com.power.common.util.UrlUtil;
import com.power.doc.constants.DocAnnotationConstants;
import com.power.doc.constants.DocGlobalConstants;
import com.power.doc.model.ApiMethodDoc;
import com.power.doc.model.ApiReturn;
import com.power.doc.model.CustomRespField;
import com.power.doc.utils.DocClassUtil;
import com.power.doc.utils.DocUtil;
import com.thoughtworks.qdox.model.*;
import com.thoughtworks.qdox.model.expression.AnnotationValue;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class JsonBuildHelper {
    private static final String JSON_CONTENT_TYPE = "application/json; charset=utf-8";

    private static final String MULTIPART_TYPE = "multipart/form-data";

    /**
     * build return json
     *
     * @param method The JavaMethod object
     * @return String
     */
    public static String buildReturnJson(JavaMethod method, Map<String, CustomRespField> responseFieldMap, JavaClassFileBuilder javaClassFileBuilder) {
        if ("void".equals(method.getReturnType().getFullyQualifiedName())) {
            return "This api return nothing.";
        }
        ApiReturn apiReturn = DocClassUtil.processReturnType(method.getReturnType().getGenericCanonicalName());
        String returnType = apiReturn.getGenericCanonicalName();
        String typeName = apiReturn.getSimpleName();
        return JsonFormatUtil.formatJson(buildJson(typeName, returnType, responseFieldMap, true, 0, new HashMap<>(), javaClassFileBuilder));
    }

    /**
     * @param typeName             type name
     * @param genericCanonicalName genericCanonicalName
     * @param responseFieldMap     map of response fields data
     * @param isResp               Response flag
     * @param counter              Recursive counter
     * @return String
     */
    private static String buildJson(String typeName, String genericCanonicalName, Map<String, CustomRespField> responseFieldMap,
                                    boolean isResp, int counter, Map<String, String> registryClasses, JavaClassFileBuilder javaClassFileBuilder) {
        if (registryClasses.containsKey(typeName) && counter > registryClasses.size()) {
            return "{\"$ref\":\"...\"}";
        }
        registryClasses.put(typeName, typeName);
        if (DocClassUtil.isMvcIgnoreParams(typeName)) {
            if (DocGlobalConstants.MODE_AND_VIEW_FULLY.equals(typeName)) {
                return "Forward or redirect to a page view.";
            } else {
                return "Error restful return.";
            }
        }
        if (DocClassUtil.isPrimitive(typeName)) {
            return StringUtil.removeQuotes(DocUtil.jsonValueByType(typeName));
        }
        StringBuilder data0 = new StringBuilder();
        JavaClass cls = javaClassFileBuilder.getModelClassByName(typeName);
        data0.append("{");
        String[] globGicName = DocClassUtil.getSimpleGicName(genericCanonicalName);
        StringBuilder data = new StringBuilder();
        if (DocClassUtil.isCollection(typeName) || DocClassUtil.isArray(typeName)) {
            data.append("[");
            if (globGicName.length == 0) {
                data.append("{\"object\":\"any object\"}");
                data.append("]");
                return data.toString();
            }
            String gNameTemp = globGicName[0];
            String gName = DocClassUtil.isArray(typeName) ? gNameTemp.substring(0, gNameTemp.indexOf("[")) : globGicName[0];
            if (DocGlobalConstants.JAVA_OBJECT_FULLY.equals(gName)) {
                data.append("{\"waring\":\"You may use java.util.Object instead of display generics in the List\"}");
            } else if (DocClassUtil.isPrimitive(gName)) {
                data.append(DocUtil.jsonValueByType(gName)).append(",");
                data.append(DocUtil.jsonValueByType(gName));
            } else if (gName.contains("<")) {
                String simple = DocClassUtil.getSimpleName(gName);
                String json = buildJson(simple, gName, responseFieldMap, isResp, counter + 1, registryClasses, javaClassFileBuilder);
                data.append(json);
            } else if (DocClassUtil.isCollection(gName)) {
                data.append("\"any object\"");
            } else {
                String json = buildJson(gName, gName, responseFieldMap, isResp, counter + 1, registryClasses, javaClassFileBuilder);
                data.append(json);
            }
            data.append("]");
            return data.toString();
        } else if (DocClassUtil.isMap(typeName)) {
            String gNameTemp = genericCanonicalName;
            String[] getKeyValType = DocClassUtil.getMapKeyValueType(gNameTemp);
            if (getKeyValType.length == 0) {
                data.append("{\"mapKey\":{}}");
                return data.toString();
            }
            if (!DocGlobalConstants.JAVA_STRING_FULLY.equals(getKeyValType[0])) {
                throw new RuntimeException("Map's key can only use String for json,but you use " + getKeyValType[0]);
            }
            String gicName = gNameTemp.substring(gNameTemp.indexOf(",") + 1, gNameTemp.lastIndexOf(">"));
            if (DocGlobalConstants.JAVA_OBJECT_FULLY.equals(gicName)) {
                data.append("{").append("\"mapKey\":").append("{\"waring\":\"You may use java.util.Object for Map value; smart-doc can't be handle.\"}").append("}");
            } else if (DocClassUtil.isPrimitive(gicName)) {
                data.append("{").append("\"mapKey1\":").append(DocUtil.jsonValueByType(gicName)).append(",");
                data.append("\"mapKey2\":").append(DocUtil.jsonValueByType(gicName)).append("}");
            } else if (gicName.contains("<")) {
                String simple = DocClassUtil.getSimpleName(gicName);
                String json = buildJson(simple, gicName, responseFieldMap, isResp, counter + 1, registryClasses, javaClassFileBuilder);
                data.append("{").append("\"mapKey\":").append(json).append("}");
            } else {
                data.append("{").append("\"mapKey\":").append(buildJson(gicName, gNameTemp, responseFieldMap, isResp, counter + 1, registryClasses, javaClassFileBuilder)).append("}");
            }
            return data.toString();
        } else if (DocGlobalConstants.JAVA_OBJECT_FULLY.equals(typeName)) {
            if (DocGlobalConstants.JAVA_OBJECT_FULLY.equals(typeName)) {
                data.append("{\"object\":\" any object\"},");
                // throw new RuntimeException("Please do not return java.lang.Object directly in api interface.");
            }
        } else {
            List<JavaField> fields = JavaClassUtil.getFields(cls);
            boolean isGenerics = checkGenerics(fields);
            int i = 0;
            out:
            for (JavaField field : fields) {
                String subTypeName = field.getType().getFullyQualifiedName();
                String fieldName = field.getName();
                if ("this$0".equals(fieldName) ||
                        "serialVersionUID".equals(fieldName) ||
                        DocClassUtil.isIgnoreFieldTypes(subTypeName)) {
                    continue;
                }
                List<DocletTag> paramTags = field.getTags();
                if (!isResp) {
                    pre:
                    for (DocletTag docletTag : paramTags) {
                        if (DocClassUtil.isIgnoreTag(docletTag.getName())) {
                            continue out;
                        }
                    }
                }
                List<JavaAnnotation> annotations = field.getAnnotations();
                for (JavaAnnotation annotation : annotations) {
                    String annotationName = annotation.getType().getSimpleName();
                    if (DocAnnotationConstants.SHORT_JSON_IGNORE.equals(annotationName) && isResp) {
                        continue out;
                    } else if (DocAnnotationConstants.SHORT_JSON_FIELD.equals(annotationName) && isResp) {
                        if (null != annotation.getProperty(DocAnnotationConstants.SERIALIZE_PROP)) {
                            if (Boolean.FALSE.toString().equals(annotation.getProperty(DocAnnotationConstants.SERIALIZE_PROP).toString())) {
                                continue out;
                            }
                        } else if (null != annotation.getProperty(DocAnnotationConstants.NAME_PROP)) {
                            fieldName = StringUtil.removeQuotes(annotation.getProperty(DocAnnotationConstants.NAME_PROP).toString());
                        }
                    } else if (DocAnnotationConstants.SHORT_JSON_PROPERTY.equals(annotationName) && isResp) {
                        if (null != annotation.getProperty(DocAnnotationConstants.VALUE_PROP)) {
                            fieldName = StringUtil.removeQuotes(annotation.getProperty(DocAnnotationConstants.VALUE_PROP).toString());
                        }
                    }
                }
                String typeSimpleName = field.getType().getSimpleName();

                String fieldGicName = field.getType().getGenericCanonicalName();
                data0.append("\"").append(fieldName).append("\":");
                if (DocClassUtil.isPrimitive(subTypeName)) {
                    CustomRespField customResponseField = responseFieldMap.get(fieldName);
                    if (null != customResponseField) {
                        Object val = customResponseField.getValue();
                        if (null != val) {
                            if ("String".equals(typeSimpleName)) {
                                data0.append("\"").append(val).append("\",");
                            } else {
                                data0.append(val).append(",");
                            }
                        } else {
                            data0.append(DocUtil.getValByTypeAndFieldName(typeSimpleName, field.getName())).append(",");
                        }
                    } else {
                        data0.append(DocUtil.getValByTypeAndFieldName(typeSimpleName, field.getName())).append(",");
                    }
                } else {
                    if (DocClassUtil.isCollection(subTypeName) || DocClassUtil.isArray(subTypeName)) {
                        fieldGicName = DocClassUtil.isArray(subTypeName) ? fieldGicName.substring(0, fieldGicName.indexOf("[")) : fieldGicName;
                        if (DocClassUtil.getSimpleGicName(fieldGicName).length == 0) {
                            data0.append("{\"object\":\"any object\"},");
                            continue out;
                        }
                        String gicName = DocClassUtil.getSimpleGicName(fieldGicName)[0];

                        if (DocGlobalConstants.JAVA_STRING_FULLY.equals(gicName)) {
                            data0.append("[").append("\"").append(buildJson(gicName, fieldGicName, responseFieldMap, isResp, counter + 1, registryClasses, javaClassFileBuilder)).append("\"]").append(",");
                        } else if (DocGlobalConstants.JAVA_LIST_FULLY.equals(gicName)) {
                            data0.append("{\"object\":\"any object\"},");
                        } else if (gicName.length() == 1) {
                            if (globGicName.length == 0) {
                                data0.append("{\"object\":\"any object\"},");
                                continue out;
                            }
                            String gicName1 = (i < globGicName.length) ? globGicName[i] : globGicName[globGicName.length - 1];
                            if (DocGlobalConstants.JAVA_STRING_FULLY.equals(gicName1)) {
                                data0.append("[").append("\"").append(buildJson(gicName1, gicName1, responseFieldMap, isResp, counter + 1, registryClasses, javaClassFileBuilder)).append("\"]").append(",");
                            } else {
                                if (!typeName.equals(gicName1)) {
                                    data0.append("[").append(buildJson(DocClassUtil.getSimpleName(gicName1), gicName1, responseFieldMap, isResp, counter + 1, registryClasses, javaClassFileBuilder)).append("]").append(",");
                                } else {
                                    data0.append("[{\"$ref\":\"..\"}]").append(",");
                                }
                            }
                        } else {
                            if (!typeName.equals(gicName)) {
                                if (DocClassUtil.isMap(gicName)) {
                                    data0.append("[{\"mapKey\":{}}],");
                                    continue out;
                                }
                                data0.append("[").append(buildJson(gicName, fieldGicName, responseFieldMap, isResp, counter + 1, registryClasses, javaClassFileBuilder)).append("]").append(",");
                            } else {
                                data0.append("[{\"$ref\":\"..\"}]").append(",");
                            }
                        }
                    } else if (DocClassUtil.isMap(subTypeName)) {
                        if (DocClassUtil.isMap(fieldGicName)) {
                            data0.append("{").append("\"mapKey\":{}},");
                            continue out;
                        }
                        String gicName = fieldGicName.substring(fieldGicName.indexOf(",") + 1, fieldGicName.indexOf(">"));
                        if (gicName.length() == 1) {
                            String gicName1 = (i < globGicName.length) ? globGicName[i] : globGicName[globGicName.length - 1];
                            if (DocGlobalConstants.JAVA_STRING_FULLY.equals(gicName1)) {
                                data0.append("{").append("\"mapKey\":\"").append(buildJson(gicName1, gicName1, responseFieldMap, isResp, counter + 1, registryClasses, javaClassFileBuilder)).append("\"},");
                            } else {
                                if (!typeName.equals(gicName1)) {
                                    data0.append("{").append("\"mapKey\":").append(buildJson(DocClassUtil.getSimpleName(gicName1), gicName1, responseFieldMap, isResp, counter + 1, registryClasses, javaClassFileBuilder)).append("},");
                                } else {
                                    data0.append("{\"mapKey\":{}},");
                                }
                            }
                        } else {
                            data0.append("{").append("\"mapKey\":").append(buildJson(gicName, fieldGicName, responseFieldMap, isResp, counter + 1, registryClasses, javaClassFileBuilder)).append("},");
                        }
                    } else if (subTypeName.length() == 1) {
                        if (!typeName.equals(genericCanonicalName)) {
                            String gicName = globGicName[i];
                            if (gicName.contains("<")) {
                                String simple = DocClassUtil.getSimpleName(gicName);
                                data0.append(buildJson(simple, gicName, responseFieldMap, isResp, counter + 1, registryClasses, javaClassFileBuilder)).append(",");
                            } else {
                                if (DocClassUtil.isPrimitive(gicName)) {
                                    data0.append(DocUtil.jsonValueByType(gicName)).append(",");
                                } else {
                                    data0.append(buildJson(gicName, gicName, responseFieldMap, isResp, counter + 1, registryClasses, javaClassFileBuilder)).append(",");
                                }
                            }
                        } else {
                            data0.append("{\"waring\":\"You may have used non-display generics.\"},");
                        }
                        i++;
                    } else if (DocGlobalConstants.JAVA_OBJECT_FULLY.equals(subTypeName)) {
                        if (isGenerics) {
                            data0.append("{\"object\":\"any object\"},");
                        } else if (i < globGicName.length) {
                            String gicName = globGicName[i];
                            if (!typeName.equals(genericCanonicalName)) {
                                if (DocClassUtil.isPrimitive(gicName)) {
                                    data0.append("\"").append(buildJson(gicName, genericCanonicalName, responseFieldMap, isResp, counter + 1, registryClasses, javaClassFileBuilder)).append("\",");
                                } else {
                                    data0.append(buildJson(gicName, gicName, responseFieldMap, isResp, counter + 1, registryClasses, javaClassFileBuilder)).append(",");
                                }
                            } else {
                                data0.append("{\"waring\":\"You may have used non-display generics.\"},");
                            }
                        } else {
                            data0.append("{\"waring\":\"You may have used non-display generics.\"},");
                        }
                        if (!isGenerics) i++;
                    } else if (typeName.equals(subTypeName)) {
                        data0.append("{\"$ref\":\"...\"}").append(",");
                    } else {
                        JavaClass javaClass = javaClassFileBuilder.getJavaProjectBuilder().getClassByName(subTypeName);
                        if (!isResp && javaClass.isEnum()) {
                            Object value = handleEnumValue(javaClass, Boolean.FALSE);
                            data0.append(value).append(",");
                        } else {
                            data0.append(buildJson(subTypeName, fieldGicName, responseFieldMap, isResp, counter + 1, registryClasses, javaClassFileBuilder)).append(",");
                        }
                    }
                }
            }
        }
        if (data0.toString().contains(",")) {
            data0.deleteCharAt(data0.lastIndexOf(","));
        }
        data0.append("}");
        return data0.toString();
    }

    public static String buildReqJson(JavaMethod method, ApiMethodDoc apiMethodDoc, Boolean isPostMethod, Map<String, CustomRespField> fieldMap, JavaClassFileBuilder javaClassFileBuilder) {
        List<JavaParameter> parameterList = method.getParameters();
        if (parameterList.size() < 1) {
            return apiMethodDoc.getUrl();
        }
        boolean containsBrace = apiMethodDoc.getUrl().contains("{");
        Map<String, String> paramsMap = new LinkedHashMap<>();
        for (JavaParameter parameter : parameterList) {
            JavaType javaType = parameter.getType();
            String simpleTypeName = javaType.getValue();
            String gicTypeName = javaType.getGenericCanonicalName();
            String typeName = javaType.getFullyQualifiedName();
            JavaClass javaClass = javaClassFileBuilder.getJavaProjectBuilder().getClassByName(typeName);
            String paraName = parameter.getName();
            if (!DocClassUtil.isMvcIgnoreParams(typeName)) {
                //file upload
                if (gicTypeName.contains(DocGlobalConstants.MULTIPART_FILE_FULLY)) {
                    apiMethodDoc.setContentType("multipart/form-data");
                    return DocClassUtil.isArray(typeName) ? "Use FormData upload files." : "Use FormData upload file.";
                }
                List<JavaAnnotation> annotations = parameter.getAnnotations();
                int requestBodyCounter = 0;
                String defaultVal = null;
                boolean notHasRequestParams = true;
                for (JavaAnnotation annotation : annotations) {
                    String fullName = annotation.getType().getFullyQualifiedName();
                    if (!fullName.contains(DocGlobalConstants.SPRING_WEB_ANNOTATION_PACKAGE)) {
                        continue;
                    }
                    String annotationName = annotation.getType().getSimpleName();
                    if (DocAnnotationConstants.REQUEST_BODY.equals(annotationName) || DocGlobalConstants.REQUEST_BODY_FULLY.equals(annotationName)) {
                        requestBodyCounter++;
                        apiMethodDoc.setContentType(JSON_CONTENT_TYPE);
                        if (DocClassUtil.isPrimitive(simpleTypeName)) {
                            StringBuilder builder = new StringBuilder();
                            builder.append("{\"")
                                    .append(paraName)
                                    .append("\":")
                                    .append(DocUtil.jsonValueByType(simpleTypeName))
                                    .append("}");
                            return builder.toString();
                        } else {
                            return buildJson(typeName, gicTypeName, fieldMap, false, 0, new HashMap<>(), javaClassFileBuilder);
                        }
                    }

                    if (DocAnnotationConstants.SHORT_REQ_PARAM.equals(annotationName)) {
                        notHasRequestParams = false;
                    }
                    AnnotationValue annotationDefaultVal = annotation.getProperty(DocAnnotationConstants.DEFAULT_VALUE_PROP);
                    if (null != annotationDefaultVal) {
                        defaultVal = StringUtil.removeQuotes(annotationDefaultVal.toString());
                    }
                    AnnotationValue annotationValue = annotation.getProperty(DocAnnotationConstants.VALUE_PROP);
                    if (null != annotationValue) {
                        paraName = StringUtil.removeQuotes(annotationValue.toString());
                    }
                    AnnotationValue annotationOfName = annotation.getProperty(DocAnnotationConstants.NAME_PROP);
                    if (null != annotationOfName) {
                        paraName = StringUtil.removeQuotes(annotationOfName.toString());
                    }
                    if (DocAnnotationConstants.REQUEST_HERDER.equals(annotationName)) {
                        paraName = null;
                    }
                }
                if (DocClassUtil.isPrimitive(typeName) && parameterList.size() == 1
                        && isPostMethod && notHasRequestParams && !containsBrace) {
                    apiMethodDoc.setContentType(JSON_CONTENT_TYPE);
                    StringBuilder builder = new StringBuilder();
                    builder.append("{\"")
                            .append(paraName)
                            .append("\":")
                            .append(DocUtil.jsonValueByType(simpleTypeName))
                            .append("}");
                    return builder.toString();
                }
                if (requestBodyCounter < 1 && paraName != null) {
                    if (javaClass.isEnum()) {
                        Object value = handleEnumValue(javaClass, Boolean.TRUE);
                        paramsMap.put(paraName, StringUtil.removeQuotes(String.valueOf(value)));
                    } else if (annotations.size() < 1 && !DocClassUtil.isPrimitive(typeName)) {
                        return "Smart-doc can't support create form-data example,It is recommended to use @RequestBody to receive parameters.";
                    } else if (StringUtil.isEmpty(defaultVal) && DocClassUtil.isPrimitive(typeName)) {
                        paramsMap.put(paraName, DocUtil.getValByTypeAndFieldName(simpleTypeName, paraName,
                                true));
                    } else {
                        paramsMap.put(paraName, defaultVal);
                    }
                }
            }
        }
        String url;
        if (containsBrace) {
            url = DocUtil.formatAndRemove(apiMethodDoc.getUrl(), paramsMap);
            url = UrlUtil.urlJoin(url, paramsMap);
        } else {
            url = UrlUtil.urlJoin(apiMethodDoc.getUrl(), paramsMap);
        }
        return url;
    }

    private static Object handleEnumValue(JavaClass javaClass, boolean returnEnum) {
        List<JavaField> javaFields = javaClass.getEnumConstants();
        Object value = null;
        int index = 0;
        for (JavaField javaField : javaFields) {
            String simpleName = javaField.getType().getSimpleName();
            StringBuilder valueBuilder = new StringBuilder();
            valueBuilder.append("\"").append(javaField.getName()).append("\"").toString();
            if (returnEnum) {
                value = valueBuilder.toString();
                return value;
            }
            if (!DocClassUtil.isPrimitive(simpleName) && index < 1) {
                if (null != javaField.getEnumConstantArguments()) {
                    value = javaField.getEnumConstantArguments().get(0);
                } else {
                    value = valueBuilder.toString();
                }
            }
            index++;
        }
        return value;
    }

    private static boolean checkGenerics(List<JavaField> fields) {
        checkGenerics:
        for (JavaField field : fields) {
            if (field.getType().getFullyQualifiedName().length() == 1) {
                return true;
            }
        }
        return false;
    }
}
