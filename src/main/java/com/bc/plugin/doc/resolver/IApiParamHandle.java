package com.bc.plugin.doc.resolver;

import com.bc.plugin.doc.builder.BuilderConfig;
import com.bc.plugin.doc.dto.FieldClassDTO;
import com.power.doc.model.ApiConfig;
import com.power.doc.model.ApiParam;

/**
 * @author xhh
 */
public interface IApiParamHandle {
    /**
     * 初始化方法
     */
    default void init(ApiConfig apiConfig, BuilderConfig builderConfig) {
    }

    /**
     * 统一结果处理
     *
     * @param apiParam 字段处理结果
     */
    default void handleApiParam(ApiParam apiParam) {
    }

    /**
     * 字段处理结果接口
     *
     * @param apiParam 字段处理结果
     * @return null 表示该字段不添加
     */
    ApiParam handle(ApiParam apiParam, FieldClassDTO fieldClassDTO, boolean isResponse);
}
