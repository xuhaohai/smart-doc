package com.bc.plugin.doc.resolver;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bc.plugin.doc.resolver.impl.NoMatchTypeResolver;
import com.power.doc.model.ApiReturn;

/**
 * @author xhh 
 */
public class ResponseTypeResolverComposite implements IResponseTypeResolver {

	private List<IResponseTypeResolver> resolvers = new ArrayList<>();
	private NoMatchTypeResolver noMatchTypeResolver = new NoMatchTypeResolver();
	private Map<String, ApiReturn> cache = new HashMap<>();

	public void addResolver(IResponseTypeResolver responseTypeResolver) {
		resolvers.add(responseTypeResolver);
	}

	@Override
	public ApiReturn processReturnType(String fullyName) {
		ApiReturn apiReturn = cache.get(fullyName);
		if (apiReturn == null) {
			apiReturn = getResolver(fullyName).processReturnType(fullyName);
			cache.put(fullyName, apiReturn);
		}
		return apiReturn;
	}

	@Override
	public boolean support(String fullyName) {
		return true;
	}

	private IResponseTypeResolver getResolver(String fullyName) {
		for (IResponseTypeResolver responseTypeResolver : resolvers) {
			if (responseTypeResolver.support(fullyName)) {
				return responseTypeResolver;
			}
		}

		return noMatchTypeResolver;
	}
}
