package com.bc.plugin.doc.resolver.impl;

import com.bc.plugin.doc.resolver.IResponseTypeResolver;
import com.power.doc.model.ApiReturn;

/**
 * @author xhh 
 */
public class NoMatchTypeResolver implements IResponseTypeResolver {
	@Override
	public boolean support(String fullyName) {
		return true;
	}

	@Override
	public ApiReturn processReturnType(String fullyName) {
		ApiReturn apiReturn = new ApiReturn();
		apiReturn.setGenericCanonicalName(fullyName);
		if (fullyName.contains("<")) {
			apiReturn.setSimpleName(fullyName.substring(0, fullyName.indexOf("<")));
		} else {
			apiReturn.setSimpleName(fullyName);
		}
		return apiReturn;
	}
}
