package com.bc.plugin.doc.resolver;

import com.power.doc.model.ApiReturn;

/**
 * @author xhh 
 */
public interface IResponseTypeResolver {

	ApiReturn processReturnType(String fullyName);
	
	boolean support(String fullyName);
}
