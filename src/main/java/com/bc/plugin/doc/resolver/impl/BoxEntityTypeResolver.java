package com.bc.plugin.doc.resolver.impl;

import com.bc.plugin.doc.resolver.IResponseTypeResolver;
import com.power.doc.constants.DocGlobalConstants;
import com.power.doc.model.ApiReturn;
import com.power.doc.utils.DocClassUtil;

/**
 * @author xhh 
 */
public class BoxEntityTypeResolver implements IResponseTypeResolver {

	@Override
	public ApiReturn processReturnType(String fullyName) {
		ApiReturn apiReturn = new ApiReturn();
		if (fullyName.contains("<")) {
            String[] strings = DocClassUtil.getSimpleGicName(fullyName);
            String newFullName = strings[0];
            if (newFullName.contains("<")) {
                apiReturn.setGenericCanonicalName(newFullName);
                apiReturn.setSimpleName(newFullName.substring(0, newFullName.indexOf("<")));
            } else {
                apiReturn.setGenericCanonicalName(newFullName);
                apiReturn.setSimpleName(newFullName);
            }
        } else {
            //directly return Java Object
            apiReturn.setGenericCanonicalName(DocGlobalConstants.JAVA_OBJECT_FULLY);
            apiReturn.setSimpleName(DocGlobalConstants.JAVA_OBJECT_FULLY);
        }
		return apiReturn;
	}

	@Override
	public boolean support(String fullyName) {
		return fullyName.startsWith("java.util.concurrent.Callable")
				|| fullyName.startsWith("java.util.concurrent.Future")
				|| fullyName.startsWith("java.util.concurrent.CompletableFuture")
				|| fullyName.startsWith("org.springframework.web.context.request.async.DeferredResult")
				|| fullyName.startsWith("org.springframework.web.context.request.async.WebAsyncTask")
				|| fullyName.startsWith("reactor.core.publisher.Mono")
				|| fullyName.startsWith("org.springframework.http.ResponseEntity");
	}

}
