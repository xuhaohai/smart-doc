package com.bc.plugin.doc.filter;

import com.thoughtworks.qdox.model.JavaClass;

/**
 * 判断该类是否是Controller接口类
 *
 * @author xhh
 */
public interface IControllerFilter {
    /**
     * is controller class
     *
     * @param cls controller class
     * @return true 解析该类，false 表示不解析
     */
    boolean isControllerClass(JavaClass cls);
}
