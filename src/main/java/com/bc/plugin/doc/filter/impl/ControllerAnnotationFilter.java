package com.bc.plugin.doc.filter.impl;

import com.bc.plugin.doc.filter.IControllerFilter;
import com.power.doc.constants.DocAnnotationConstants;
import com.power.doc.constants.DocGlobalConstants;
import com.thoughtworks.qdox.model.JavaAnnotation;
import com.thoughtworks.qdox.model.JavaClass;

import java.util.List;

public class ControllerAnnotationFilter implements IControllerFilter {
    @Override
    public boolean isControllerClass(JavaClass cls) {
        List<JavaAnnotation> classAnnotations = cls.getAnnotations();
        for (JavaAnnotation annotation : classAnnotations) {
            String annotationName = annotation.getType().getName();
            if (DocAnnotationConstants.SHORT_CONTROLLER.equals(annotationName)
                    || DocAnnotationConstants.SHORT_REST_CONTROLLER.equals(annotationName)
                    || DocGlobalConstants.REST_CONTROLLER_FULLY.equals(annotationName)
                    || DocGlobalConstants.CONTROLLER_FULLY.equals(annotationName)) {
                return true;
            }
        }
        return false;
    }
}
