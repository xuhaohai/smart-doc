package com.bc.plugin.doc.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RequestMappingDTO {
    private String url;
    private String shortUrl;
    private String methodType;
    private String mediaType;
    private boolean postMethod;

}
