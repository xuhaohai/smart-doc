package com.bc.plugin.doc.dto;

import com.power.common.util.Assert;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 示例数据实体
 */
@Data
@NoArgsConstructor
public class UsageDataDTO {

    /**
     * 请求路径，唯一key
     */
    private String url;
    /**
     * 请求示例数据
     */
    private String requestData;
    /**
     * 响应示例数据
     */
    private String responseData;

    public UsageDataDTO(String url, String requestData, String responseData) {
        super();
        Assert.notNull(url, "url must not be null");
        Assert.notNull(requestData, "requestData must not be null");
        Assert.notNull(responseData, "responseData must not be null");
        this.url = url;
        this.requestData = requestData;
        this.responseData = responseData;
    }

}
