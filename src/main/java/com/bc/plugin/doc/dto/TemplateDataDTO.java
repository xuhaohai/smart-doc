package com.bc.plugin.doc.dto;

import com.power.common.util.CollectionUtil;
import com.power.common.util.DateTimeUtil;
import com.power.doc.constants.DocGlobalConstants;
import com.power.doc.constants.DocLanguage;
import com.power.doc.constants.TemplateVariable;
import com.power.doc.model.ApiDoc;
import com.power.doc.model.ApiDocDict;
import com.power.doc.model.ApiErrorCode;
import com.power.doc.model.RevisionLog;
import lombok.Builder;
import lombok.Data;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Builder
@Data
public class TemplateDataDTO {
    private static final long now = System.currentTimeMillis();

    private boolean allInOne;
    private List<ApiDoc> apiDocList;
    private List<ApiDocDict> apiDocDictList;
    private List<ApiErrorCode> errorCodes;
    private List<RevisionLog> revisionLogs;
    private String outPath;
    private String projectName;
    private String fileName;
    private DocLanguage docLanguage;

    public Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();
        map.put(TemplateVariable.API_DOC_LIST.getVariable(), apiDocList);
        map.put(TemplateVariable.ERROR_CODE_LIST.getVariable(), errorCodes);
        map.put(TemplateVariable.VERSION_LIST.getVariable(), revisionLogs);

        map.put(TemplateVariable.VERSION.getVariable(), now);
        String strTime = DateTimeUtil.long2Str(now, DateTimeUtil.DATE_FORMAT_SECOND);
        map.put(TemplateVariable.CREATE_TIME.getVariable(), strTime);
        map.put(TemplateVariable.PROJECT_NAME.getVariable(), projectName);
        if (CollectionUtil.isEmpty(errorCodes)) {
            map.put(TemplateVariable.DICT_ORDER.getVariable(), apiDocList.size() + 1);
        } else {
            map.put(TemplateVariable.DICT_ORDER.getVariable(), apiDocList.size() + 2);
        }
        if (null != docLanguage) {
            if (DocLanguage.CHINESE.code.equals(docLanguage.getCode())) {
                map.put(TemplateVariable.ERROR_LIST_TITLE.getVariable(), DocGlobalConstants.ERROR_CODE_LIST_CN_TITLE);
                map.put(TemplateVariable.DICT_LIST_TITLE.getVariable(), DocGlobalConstants.DICT_CN_TITLE);
            } else {
                map.put(TemplateVariable.ERROR_LIST_TITLE.getVariable(), DocGlobalConstants.ERROR_CODE_LIST_EN_TITLE);
                map.put(TemplateVariable.DICT_LIST_TITLE.getVariable(), DocGlobalConstants.DICT_EN_TITLE);
            }
        } else {
            map.put(TemplateVariable.ERROR_LIST_TITLE.getVariable(), DocGlobalConstants.ERROR_CODE_LIST_CN_TITLE);
            map.put(TemplateVariable.DICT_LIST_TITLE.getVariable(), DocGlobalConstants.DICT_CN_TITLE);
        }
        map.put(TemplateVariable.DICT_LIST.getVariable(), apiDocDictList);
        return map;
    }
}
