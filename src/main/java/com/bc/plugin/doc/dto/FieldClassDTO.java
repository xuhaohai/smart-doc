package com.bc.plugin.doc.dto;

import com.thoughtworks.qdox.model.JavaField;
import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class FieldClassDTO {
    /**
     * 所属类名，带泛型全名类型
     */
    private String genericCanonicalName;
    /**
     * 所属类的字段
     */
    private JavaField javaField;
    private int level;
}
