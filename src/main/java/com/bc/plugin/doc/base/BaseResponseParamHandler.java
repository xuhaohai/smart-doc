package com.bc.plugin.doc.base;

import java.util.List;

import com.bc.plugin.doc.resolver.ApiParamResolver;
import com.bc.plugin.doc.resolver.IResponseTypeResolver;
import com.bc.plugin.doc.resolver.ResponseTypeResolverComposite;
import com.power.doc.model.ApiParam;
import com.power.doc.model.ApiReturn;
import com.thoughtworks.qdox.model.JavaMethod;

import lombok.Setter;

/**
 * @author xhh 2019/11/21
 */
public abstract class BaseResponseParamHandler {
    protected ResponseTypeResolverComposite responseTypeResolverComposite = new ResponseTypeResolverComposite();

    public abstract List<ApiParam> handle(ApiReturn apiReturn, ApiParamResolver apiParamResolver);

    public List<ApiParam> getResponseParams(JavaMethod method, ApiParamResolver apiParamResolver) {
        if (!"void".equals(method.getReturnType().getFullyQualifiedName())) {
            ApiReturn apiReturn = processReturnType(method.getReturnType().getGenericCanonicalName());
            return handle(apiReturn, apiParamResolver);
        }
        return null;
    }

    public ApiReturn processReturnType(String canonicalName) {
        return responseTypeResolverComposite.processReturnType(canonicalName);
    }

    public void addTypeResolver(IResponseTypeResolver responseTypeResolver) {
        responseTypeResolverComposite.addResolver(responseTypeResolver);
    }

}
