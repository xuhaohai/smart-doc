package com.bc.plugin.doc.base;

import com.bc.plugin.doc.dto.RequestMappingDTO;
import com.thoughtworks.qdox.model.JavaMethod;

/**
 * @author xhh 2019/11/21
 */
public interface IRequestMappingHandler {
	RequestMappingDTO handle(String host, String preUrl, JavaMethod method);
}
