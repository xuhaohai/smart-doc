package com.bc.plugin.doc.base;

import com.power.doc.constants.DocTags;
import com.power.doc.model.ApiReqHeader;
import com.power.doc.utils.DocUtil;
import com.thoughtworks.qdox.model.JavaMethod;
import com.thoughtworks.qdox.model.JavaParameter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
*
* @author xhh 2019/11/21
*/
public interface IRequestHeaderHandler {

    ApiReqHeader handle(JavaParameter javaParameter, Map<String, String> paramsComments);

    default List<ApiReqHeader> getApiReqHeaders(JavaMethod method, List<ApiReqHeader> globalHeaders) {
        List<ApiReqHeader> apiReqHeaders = new ArrayList<>();
        for (JavaParameter javaParameter : method.getParameters()) {
            String className = method.getDeclaringClass().getCanonicalName();
            Map<String, String> paramMap = DocUtil.getParamsComments(method, DocTags.PARAM, className);
            ApiReqHeader apiReqHeader = handle(javaParameter, paramMap);
            if (apiReqHeader != null) {
                apiReqHeaders.add(apiReqHeader);
            }
        }
        if (globalHeaders != null) {
            apiReqHeaders = Stream.of(globalHeaders, apiReqHeaders)
                    .flatMap(Collection::stream)
                    .distinct()
                    .collect(Collectors.toList());
        }
        return apiReqHeaders;
    }

}
