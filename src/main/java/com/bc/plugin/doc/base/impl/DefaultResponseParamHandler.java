package com.bc.plugin.doc.base.impl;

import com.bc.plugin.doc.base.BaseResponseParamHandler;
import com.bc.plugin.doc.resolver.ApiParamResolver;
import com.bc.plugin.doc.resolver.impl.BoxEntityTypeResolver;
import com.bc.plugin.doc.resolver.impl.FluxTypeResolver;
import com.power.doc.model.ApiParam;
import com.power.doc.model.ApiReturn;
import com.power.doc.utils.DocClassUtil;

import java.util.List;

/**
 * @author xhh 2019/11/21
 */
public class DefaultResponseParamHandler extends BaseResponseParamHandler {

    public DefaultResponseParamHandler() {
        responseTypeResolverComposite.addResolver(new FluxTypeResolver());
        responseTypeResolverComposite.addResolver(new BoxEntityTypeResolver());
    }

    @Override
    public List<ApiParam> handle(ApiReturn apiReturn, ApiParamResolver apiParamResolver) {
        String returnType = apiReturn.getGenericCanonicalName();
        String typeName = apiReturn.getSimpleName();
        if (DocClassUtil.isMvcIgnoreParams(typeName)) {
            return null;
        }
        return apiParamResolver.resolverResponseParams(returnType);
    }

}
