package com.bc.plugin.doc.base;

import com.bc.plugin.doc.resolver.ApiParamResolver;
import com.power.doc.constants.DocTags;
import com.power.doc.model.ApiConfig;
import com.power.doc.model.ApiParam;
import com.power.doc.utils.DocUtil;
import com.thoughtworks.qdox.model.JavaClass;
import com.thoughtworks.qdox.model.JavaMethod;
import com.thoughtworks.qdox.model.JavaParameter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author xhh 2019/11/21
 */
public abstract class BaseRequestParamHandler {
    public abstract List<ApiParam> handle(JavaParameter parameter, Map<String, String> paramsComments, ApiParamResolver apiParamResolver, boolean isStrict);

    public List<ApiParam> getRequestParams(JavaMethod method, JavaClass cls, ApiConfig config, ApiParamResolver apiParamResolver) {
        List<JavaParameter> parameterList = method.getParameters();
        if (!parameterList.isEmpty()) {
            List<ApiParam> paramListAll = new ArrayList<>();
            Map<String, String> paramTagMap = DocUtil.getParamsComments(method, DocTags.PARAM, cls.getCanonicalName());
            for (JavaParameter parameter : parameterList) {
                List<ApiParam> paramList = handle(parameter, paramTagMap, apiParamResolver, config.isStrict());
                if (paramList != null) {
                    paramListAll.addAll(paramList);
                }
            }
            return paramListAll;
        }
        return null;
    }
}
