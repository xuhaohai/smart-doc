package com.bc.plugin.doc.base;

import com.bc.plugin.doc.dto.UsageDataDTO;

import lombok.Setter;

public abstract class BaseUsageDataHandler {

	@Setter
	protected IUsageDataRepository usageDataRepository;

	public String getRequestUsageData(String url) {
		UsageDataDTO exampleDataDTO = usageDataRepository.findByUrl(url);
		if (exampleDataDTO != null) {
			return exampleDataDTO.getRequestData();
		}
		return null;
	}

	public String getResponseUsageData(String url) {
		UsageDataDTO exampleDataDTO = usageDataRepository.findByUrl(url);
		if (exampleDataDTO != null) {
			return exampleDataDTO.getResponseData();
		}
		return null;
	}

}
