package com.bc.plugin.doc.base;

import com.bc.plugin.doc.dto.UsageDataDTO;

/**
 * @author xhh
 * 示例数据接口类，url为标识
 */
public interface IUsageDataRepository {
    /**
     * 保存示例数据
     */
    void save(UsageDataDTO usageDataDTO);


    /**
     * 获取示例数据
     */
    UsageDataDTO findByUrl(String url);

    default void saveRequestData(String url, String requestData) {
        UsageDataDTO exampleDataDTO = findByUrl(url);
        if (exampleDataDTO == null) {
            exampleDataDTO = new UsageDataDTO();
            exampleDataDTO.setUrl(url);
        }
        exampleDataDTO.setRequestData(requestData);
        save(exampleDataDTO);
    }

    default void saveResponseData(String url, String responseData) {
        UsageDataDTO exampleDataDTO = findByUrl(url);
        if (exampleDataDTO == null) {
            exampleDataDTO = new UsageDataDTO();
            exampleDataDTO.setUrl(url);
        }
        exampleDataDTO.setResponseData(responseData);
        save(exampleDataDTO);
    }
}
